//package com.taskapi.manufacture.service;
//
//import com.taskapi.manufacture.exception.RecordNotFound;
//import com.taskapi.manufacture.model.dto.CustomerReqDto;
//import com.taskapi.manufacture.model.dto.CustomerRespDto;
//
//import lombok.extern.slf4j.Slf4j;
//
//import org.apache.http.HttpResponse;
//import org.apache.http.HttpStatus;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.client.methods.HttpUriRequest;
//import org.apache.http.impl.client.HttpClientBuilder;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.web.server.LocalServerPort;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import javax.transaction.Transactional;
//import java.io.IOException;
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//
//@Slf4j
//public class CustomerServiceTest {
//    @Autowired
//    CustomerService customerService;
//
//    @LocalServerPort
//    int port;
//
////    @Rule
////    public WireMockRule wireMockRule = new WireMockRule(8089);
//    @Test
//    public void findAll(){
//        List<CustomerReqDto> customer = customerService.findAll();
//        log.info("======= LIST CUSTOMER =======");
//        for (int i = 0; i < customer.size();i++){
//            log.info("Id : " + (i+1));
//            log.info("Customer Name : " + customer.get(i).getCompanyName());
//            log.info("Account Balance : " + customer.get(i).getAccountBalance());
//        }
//
//        log.info("==========================");
//        assertThat(customer.size()).isNotNull();
//    }
//
//    @Test
//    @Transactional
//    public void findById(){
//        long Id = 1;
//        CustomerReqDto customerReqDto = customerService.findById(Id);
//        log.info("Customer ID     : " + customerReqDto.getCustomerId());
//        log.info("Customer Name   : " + customerReqDto.getCompanyName());
//        log.info("Account Balance : " + customerReqDto.getAccountBalance());
//        log.info("Status          : " + customerReqDto.getStatus());
//
//        assertThat(customerReqDto.getCustomerId())
//                .isEqualTo(Id);
//
//        assertThat(customerReqDto)
//                .hasFieldOrProperty("customerId")
//                .hasFieldOrProperty("customerName")
//                .hasFieldOrProperty("status")
//                .hasFieldOrProperty("accountBalance");
//    }
//
//    @Test
//    public void create(){
//        List<CustomerReqDto> customerReqDtoList = customerService.findAll();
//        log.info("======= BEFORE =======");
//        log.info("StockWebClientController List Size : " + customerReqDtoList.size());
//        log.info("======================");
//
//        CustomerRespDto customerRespDto = new CustomerRespDto();
//        String customerName  = "PT. DOKU";
//        int accountBalance = 350000000;
//        String status = "AKTIF";
//
//        customerRespDto.setCompanyName(customerName);
//        customerRespDto.setAccountBalance(accountBalance);
//        customerRespDto.setStatus(status);
//
//
//        customerService.create(customerRespDto);
//        List<CustomerReqDto> customerReqDtoListAfter = customerService.findAll();
//
//        log.info("======= AFTER =======");
//        log.info("StockWebClientController List Size : " + customerReqDtoListAfter.size());
//        log.info("=====================");
//        int k = customerReqDtoListAfter.size() - 1;
//
//        log.info("Customer ID     : " + customerReqDtoListAfter.get(k).getCustomerId());
//        log.info("Customer Name   : " + customerReqDtoListAfter.get(k).getCompanyName());
//        log.info("Account Balance : " + customerReqDtoListAfter.get(k).getAccountBalance());
//        log.info("Status          : " + customerReqDtoListAfter.get(k).getStatus());
//        log.info("=====================");
//
//        assertThat(customerReqDtoListAfter.get(k).getCompanyName())
//                .isEqualTo(customerName);
//        assertThat(customerReqDtoListAfter.get(k).getAccountBalance())
//                .isEqualTo(accountBalance);
//        assertThat(customerReqDtoListAfter.get(k).getStatus())
//                .isEqualTo(status);
//
//        assertThat(customerReqDtoListAfter.size())
//                .isGreaterThan(customerReqDtoList.size());
//    }
//
//    @Test
//    @Transactional
//    public void update(){
//
//        CustomerRespDto customerRespDto = new CustomerRespDto();
//        long Id = 2;
//        CustomerReqDto customerReqDto = customerService.findById(Id);
//
//        int accountBalance = 450000000;
//        log.info("======= BEFORE =======");
//        log.info("accountBalance :" + customerReqDto.getAccountBalance());
//        customerRespDto.setAccountBalance(accountBalance);
//        CustomerRespDto customerRespDtoUpdate = customerService.update(Id, customerRespDto);
//        log.info("======= AFTER =======");
//        log.info("accountBalance : " + customerRespDtoUpdate.getAccountBalance());
//        log.info("=====================");
//
//        assertThat(customerRespDtoUpdate.getAccountBalance())
//                .isEqualTo(accountBalance);
//    }
//
//    @Test
//    public void delete(){
//        long Id = 3;
//        log.info("======= BEFORE =======");
//        List<CustomerReqDto> customerReqDtoList = customerService.findAll();
//        log.info("size = " + customerReqDtoList.size());
//        log.info("Delete Process : ");
//        customerService.delete(Id);
//        log.info("Data deleted");
//        log.info("======= AFTER =======");
//        List<CustomerReqDto> customerReqDtoListAfter = customerService.findAll();
//        log.info("size = " + customerReqDtoListAfter.size());
//        log.info("=====================");
//
//        assertThat(customerReqDtoList.size())
//                .isGreaterThan(customerReqDtoListAfter.size());
//    }
//
//    @Test(expected = RecordNotFound.class)
//    public void findByIdFailed(){
//        long id = 20;
//        CustomerReqDto customer = customerService.findById(id);
//
//        assertThat(customer.getCustomerId())
//                .isNotEqualTo(id);
//    }
//
//    @Test
//    public void ErrorResponse() throws ClientProtocolException, IOException {
//
//        int name = 6;
//        HttpUriRequest req = new HttpGet("http://localhost:"+port+"/task/customer/" + name);
//
//        HttpResponse resp = HttpClientBuilder.create().build().execute(req);
//
//        assertThat(resp.getStatusLine().getStatusCode())
//                .isEqualTo(HttpStatus.SC_NOT_FOUND);
//
//    }
//
//}
