//package com.taskapi.manufacture.service;
//
//import com.taskapi.manufacture.model.dto.StockReqDto;
//import com.taskapi.manufacture.model.dto.StockRespDto;
//
//import lombok.extern.slf4j.Slf4j;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
//@Slf4j
//public class StockServiceTest {
//    @Autowired
//    StockService stockService;
//
//    @Test
//    public void findAll(){
//        List<StockReqDto> stockReqDtoList = stockService.findAll();
//
//        log.info("======= LIST STOCK =======");
//        for (int i = 0; i < stockReqDtoList.size();i++){
//            log.info("StockWebClientController Id     : " + stockReqDtoList.get(i).getStockId());
//            log.info("StockWebClientController Name   : " + stockReqDtoList.get(i).getName());
//            log.info("Qty StockWebClientController    : " + stockReqDtoList.get(i).getQtyStock());
//            log.info("Measure Unit : " + stockReqDtoList.get(i).getMeasurementUnit());
//            log.info("Unit Price   : " + stockReqDtoList.get(i).getUnitPrice());
//            log.info("Status       : " + stockReqDtoList.get(i).getStatus());
//            log.info("Storage Loc  : " + stockReqDtoList.get(i).getStorageLocation());
//        }
//
//        log.info("==========================");
//        assertThat(stockReqDtoList.size()).isNotNull();
//    }
//
//    @Test
//    @Transactional
//    public void findById(){
//        int Id = 1;
//        StockReqDto stockReqDto = stockService.findById(new Long(Id));
//        log.info("StockWebClientController Id     : " + stockReqDto.getStockId());
//        log.info("StockWebClientController Name   : " + stockReqDto.getName());
//        log.info("Qty StockWebClientController    : " + stockReqDto.getQtyStock());
//        log.info("Measure Unit : " + stockReqDto.getMeasurementUnit());
//        log.info("Unit Price   : " + stockReqDto.getUnitPrice());
//        log.info("Status       : " + stockReqDto.getStatus());
//        log.info("Storage Loc  : " + stockReqDto.getStorageLocation());
//
//        assertThat(stockReqDto.getStockId())
//                .isEqualTo(Id);
//        assertThat(stockReqDto)
//                .hasFieldOrProperty("stockId")
//                .hasFieldOrProperty("name")
//                .hasFieldOrProperty("qtyStock")
//                .hasFieldOrProperty("measurementUnit")
//                .hasFieldOrProperty("status")
//                .hasFieldOrProperty("unitPrice")
//                .hasFieldOrProperty("storageLocation");
//
//    }
//
//    @Test
//    public void create(){
//        List<StockReqDto> stockReqDtoList = stockService.findAll();
//        log.info("======= BEFORE =======");
//        log.info("StockWebClientController List Size : " + stockReqDtoList.size());
//        log.info("======================");
//
//        StockRespDto stockRespDto = new StockRespDto();
//
//        String stockName  = "I Pad Pro";
//        int qtyStock = 44;
//        String measureUnit = "PCS";
//        int unitPrice = 12000000;
//        String storageLoc = "JKT";
//        String status = "New";
//
//        stockRespDto.setName(stockName);
//        stockRespDto.setQtyStock(qtyStock);
//        stockRespDto.setMeasurementUnit(measureUnit);
//        stockRespDto.setUnitPrice(unitPrice);
//        stockRespDto.setStorageLocation(storageLoc);
//        stockRespDto.setStatus(status);
//
//        StockRespDto stockRespDtoCreate = stockService.create(stockRespDto);
//        List<StockReqDto> stockReqDtoListAfter = stockService.findAll();
//
//        log.info("======= AFTER =======");
//        log.info("StockWebClientController List Size : " + stockReqDtoListAfter.size());
//        log.info("=====================");
//        int j = stockReqDtoListAfter.size() - 1;
//
//        log.info("StockWebClientController Id     : " + stockReqDtoListAfter.get(j).getStockId());
//        log.info("StockWebClientController Name   : " + stockReqDtoListAfter.get(j).getName());
//        log.info("Qty StockWebClientController    : " + stockReqDtoListAfter.get(j).getQtyStock());
//        log.info("Measure Unit : " + stockReqDtoListAfter.get(j).getMeasurementUnit());
//        log.info("Unit Price   : " + stockReqDtoListAfter.get(j).getUnitPrice());
//        log.info("Status       : " + stockReqDtoListAfter.get(j).getStatus());
//        log.info("Storage Loc  : " + stockReqDtoListAfter.get(j).getStorageLocation());
//        log.info("=====================");
//
//        assertThat(stockRespDtoCreate.getName())
//                .isEqualTo(stockName);
//        assertThat(stockRespDtoCreate.getQtyStock())
//                .isEqualTo(qtyStock);
//        assertThat(stockRespDtoCreate.getMeasurementUnit())
//                .isEqualTo(measureUnit);
//        assertThat(stockRespDtoCreate.getUnitPrice())
//                .isEqualTo(unitPrice);
//        assertThat(stockRespDtoCreate.getStorageLocation())
//                .isEqualTo(storageLoc);
//        assertThat(stockRespDtoCreate.getStatus())
//                .isEqualTo(status);
//
//        assertThat(stockReqDtoListAfter.size())
//                .isGreaterThan(stockReqDtoList.size());
//    }
//
//    @Test
//    @Transactional
//    public void update(){
//        StockRespDto stockRespDto = new StockRespDto();
//        long Id = 2;
//        StockReqDto stockReqDto = stockService.findById(Id);
//
//        int qtyStock = 430;
//        log.info("======= BEFORE =======");
//        log.info("Quantity StockWebClientController :" + stockReqDto.getQtyStock());
//        stockRespDto.setQtyStock(qtyStock);
//        StockRespDto stockRespDtoUpdate = stockService.update(Id, stockRespDto); //Update method Running
//        log.info("======= AFTER =======");
//        log.info("Quantity StockWebClientController : " + stockRespDtoUpdate.getQtyStock());
//        log.info("=====================");
//
//        assertThat(stockRespDtoUpdate.getQtyStock())
//                .isEqualTo(qtyStock);
//    }
//
//    @Test
//    @Transactional
//    public void delete(){
//        long Id = 3;
//        log.info("======= BEFORE =======");
//        List<StockReqDto> stockReqDtoList = stockService.findAll();
//        log.info("size = " + stockReqDtoList.size());
//        log.info("Delete Process : ");
//        stockService.delete(Id); //Delete method Running
//        log.info("Data deleted");
//        log.info("======= AFTER =======");
//        List<StockReqDto> stockReqDtoListAfter = stockService.findAll();
//        log.info("size = " + stockReqDtoListAfter.size());
//        log.info("=====================");
//
//        assertThat(stockReqDtoList.size())
//                .isGreaterThan(stockReqDtoListAfter.size());
//    }
//
//}
