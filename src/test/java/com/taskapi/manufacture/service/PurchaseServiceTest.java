//package com.taskapi.manufacture.service;
//
//import com.taskapi.manufacture.model.dto.PurchaseReqDto;
//import com.taskapi.manufacture.model.dto.PurchaseRespDto;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
//@Slf4j
//public class PurchaseServiceTest {
//    @Autowired
//    PurchaseService purchaseService;
//
//    @Test
//    public void findAll(){
//        List<PurchaseReqDto> purchaseReqDtoList = purchaseService.findAll();
//        log.info("======= LIST STOCK =======");
//        for (int i = 0; i < purchaseReqDtoList.size();i++){
//            log.info("Purchase Id  : " + purchaseReqDtoList.get(i).getPurchaseId());
////            log.info("Customer Id  : " + purchaseReqDtoList.get(i).getCustomerId());
//            log.info("StockWebClientController Id     : " + purchaseReqDtoList.get(i).getStockId());
//            log.info("Qty Purchase : " + purchaseReqDtoList.get(i).getQtyPurchase());
//            log.info("Total Price  : " + purchaseReqDtoList.get(i).getTotalPrice());
//            log.info("Created Date : " + purchaseReqDtoList.get(i).getCreatedDate());
//
//            assertThat(purchaseReqDtoList.size()).isNotNull();
//        }
//        log.info("==========================");
//    }
//
//    @Test
//    @Transactional
//    public void findById(){
//        long Id = 1;
//        PurchaseReqDto purchaseReqDto = purchaseService.findById(Id);
//        log.info("Purchase Id  : " + purchaseReqDto.getPurchaseId());
////        log.info("Customer Id  : " + purchaseReqDto.getCustomerId());
//        log.info("StockWebClientController Id     : " + purchaseReqDto.getStockId());
//        log.info("Qty Purchase : " + purchaseReqDto.getQtyPurchase());
//        log.info("Total Price  : " + purchaseReqDto.getTotalPrice());
//        log.info("Created Date : " + purchaseReqDto.getCreatedDate());
//
//        assertThat(purchaseReqDto.getPurchaseId())
//                .isEqualTo(Id);
//    }
//
//    @Test
//    @Transactional
//    public void create(){
//        List<PurchaseReqDto> purchaseReqDtoList = purchaseService.findAll();
//
//        log.info("======= BEFORE =======");
//        log.info("Purchase List Size : " + purchaseReqDtoList.size());
//
//        PurchaseRespDto purchaseRespDto = new PurchaseRespDto();
//
//        String username = "kalbio";
//        long stockId = 1;
//        int qtyPurchase = 10;
//
//        purchaseRespDto.setUsername(username);
//        purchaseRespDto.setStockId(stockId);
//        purchaseRespDto.setQtyPurchase(qtyPurchase);
//
//        PurchaseRespDto purchaseRespDtoCreate = purchaseService.create(purchaseRespDto);
//        List<PurchaseReqDto> purchaseReqDtoListAfter = purchaseService.findAll();
//
//        log.info("======= AFTER =======");
//        log.info("Purchase List Size : " + purchaseReqDtoListAfter.size());
//        log.info("=====================");
//        int i = purchaseReqDtoListAfter.size() - 1;
//
//        log.info("Purchase Id  : " + purchaseReqDtoListAfter.get(i).getPurchaseId());
////        log.info("Customer Id  : " + purchaseReqDtoListAfter.get(i).getCustomerId());
//        log.info("StockWebClientController Id     : " + purchaseReqDtoListAfter.get(i).getStockId());
//        log.info("Qty Purchase : " + purchaseReqDtoListAfter.get(i).getQtyPurchase());
//        log.info("Total Price  : " + purchaseReqDtoListAfter.get(i).getTotalPrice());
//        log.info("Created Date : " + purchaseReqDtoListAfter.get(i).getCreatedDate());
//        log.info("=====================");
//
//        assertThat(purchaseRespDtoCreate.getUsername())
//                .isEqualTo(username);
//        assertThat(purchaseRespDtoCreate.getStockId())
//                .isEqualTo(stockId);
//        assertThat(purchaseRespDtoCreate.getQtyPurchase())
//                .isEqualTo(qtyPurchase);
//
//        assertThat(purchaseRespDtoCreate.getTotalPrice())
//                .isNotNull();
//
//        assertThat(purchaseReqDtoListAfter.size())
//                .isGreaterThan(purchaseReqDtoList.size());
//
//
//    }
//
//}
