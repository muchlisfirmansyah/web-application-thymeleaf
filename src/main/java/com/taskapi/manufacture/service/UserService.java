package com.taskapi.manufacture.service;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.Role;
import com.taskapi.manufacture.model.Users;
import com.taskapi.manufacture.model.dto.CustomerRespDto;
import com.taskapi.manufacture.model.dto.UserReqDto;
import com.taskapi.manufacture.model.dto.UserRespDto;
import com.taskapi.manufacture.model.mapper.UserRespMapper;
import com.taskapi.manufacture.repository.RoleRepo;
import com.taskapi.manufacture.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;

@Service
public class UserService {

    @Autowired
    UserRepo userRepo;

    @Autowired
    RoleRepo roleRepo;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UserRespMapper userRespMapper;

    public Users findByUsername(String username){
        return userRepo.findByUsername(username);
    }

    public UserRespDto userCreate(UserRespDto userRespDto) {
        userRespDto.setPassword(bCryptPasswordEncoder.encode(userRespDto.getPassword()));
        userRespDto.setActive(true);
        Role userRole = roleRepo.findByRole(userRespDto.getRole().toString());
        userRespDto.setRole(new HashSet<Role>(Arrays.asList(userRole)));
        Users userCreate = userRespMapper.map(userRespDto, Users.class);
        Users userSaved = userRepo.save(userCreate);

        return userRespMapper.map(userSaved, UserRespDto.class);
    }

}
