package com.taskapi.manufacture.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.Stock;
import com.taskapi.manufacture.model.dto.*;
import com.taskapi.manufacture.model.Purchase;
import com.taskapi.manufacture.exception.RecordNotFound;
import com.taskapi.manufacture.model.mapper.PurchaseReqMapper;
import com.taskapi.manufacture.model.mapper.PurchaseRespMapper;
import com.taskapi.manufacture.repository.CustomerRepo;
import com.taskapi.manufacture.repository.PurchaseRepo;

import com.taskapi.manufacture.repository.StockRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * This is a Javadoc comment
 */
@Service
public class PurchaseService {

    @Autowired
    PurchaseRepo purchaseRepo;

    @Autowired
    StockRepo stockRepo;

    @Autowired
    CustomerRepo customerRepo;

    @Autowired
    StockService stockService;

    @Autowired
    CustomerService customerService;

    @Autowired
    PurchaseReqMapper purchaseReqMapper;

    @Autowired
    PurchaseRespMapper purchaseRespMapper;


    /**
     * @Service
     * This is a Javadoc comment
     * Get Data from Purchase
     * @return purchaseReqDtoList
     */
    public List<PurchaseReqDto> findAll() {
        List<Purchase> purchaseList = purchaseRepo.findAll();
        List<PurchaseReqDto> purchaseReqDtoList = new ArrayList<>();
        for (Purchase purchase : purchaseList) {

            PurchaseReqDto purchaseReqDto = purchaseReqMapper.map(purchase, PurchaseReqDto.class);
            purchaseReqDtoList.add(purchaseReqDto);
        }
        return purchaseReqDtoList;
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Get Data by purchaseId from Purchase
     * @param purchaseId
     * @return purchaseReqMapper
     */
    public PurchaseReqDto findById(Long purchaseId) {
        Optional<Purchase>optionalPurchase = purchaseRepo.findById(purchaseId);
        if (!optionalPurchase.isPresent())
            throw new RecordNotFound("Purchase ID : " + purchaseId + " Not Found");

        Purchase purchase = purchaseRepo.getOne(purchaseId);
        return purchaseReqMapper.map(purchase, PurchaseReqDto.class);
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Create Payment
     * @param purchaseRespDto
     * @return purchaseRespMapper
     */
    public PurchaseRespDto create(PurchaseRespDto purchaseRespDto) {

        StockReqDto stockReqDto = stockService.findById(purchaseRespDto.getStockId());

        purchaseRespDto.setCreatedDate(new Date());

        int getUnitPrice = stockReqDto.getUnitPrice();
        int getQtyPurchase = purchaseRespDto.getQtyPurchase();
        int totalPrice = getUnitPrice * getQtyPurchase;
        purchaseRespDto.setTotalPrice(totalPrice);

        //Update StockWebClientController
        int qtyStock = stockReqDto.getQtyStock();
        int stockNew = qtyStock - getQtyPurchase;
        if (stockNew < 0) {
            throw new IllegalArgumentException("StockWebClientController purchases exceed the available inventory, Purchase couldn't be created");
        }
        Stock stock = stockRepo.getOne(purchaseRespDto.getStockId());
        stock.setQtyStock(stockNew);

        //Update Balance
        Customer customer= customerService.findByUsername(purchaseRespDto.getUsername());
        int balanceCurr = customer.getAccountBalance();
        int balanceNew = balanceCurr - totalPrice;
        if (balanceNew < 0) {
            throw new IllegalArgumentException("Insufficient account balance to make a purchase, Purchase couldn't be created");
        }
        Customer customerSetBalance = customerRepo.findByUsername(purchaseRespDto.getUsername());
        customerSetBalance.setAccountBalance(balanceNew);

        Purchase purchase = purchaseRespMapper.map(purchaseRespDto, Purchase.class);
        Purchase purchaseSaved =  purchaseRepo.save(purchase);

        return purchaseRespMapper.map(purchaseSaved, PurchaseRespDto.class);

    }
}
