package com.taskapi.manufacture.service;

import java.util.ArrayList;
import java.util.List;

import java.util.Optional;

import com.taskapi.manufacture.model.dto.StockReqDto;
import com.taskapi.manufacture.model.dto.StockRespDto;
import com.taskapi.manufacture.model.Stock;
import com.taskapi.manufacture.exception.RecordNotFound;
import com.taskapi.manufacture.model.mapper.StockReqMapper;
import com.taskapi.manufacture.model.mapper.StockRespMapper;
import com.taskapi.manufacture.repository.StockRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * This is a Javadoc comment
 */
@Service
public class StockService {
    private static final String STOCK_NOT_FOUND = "StockWebClientController ID : ";
    @Autowired
    StockRepo stockRepo;

    @Autowired
    StockReqMapper stockReqMapper;

    @Autowired
    StockRespMapper stockRespMapper;

    /**
     * @Service
     * This is a Javadoc comment
     * Get Data from StockWebClientController
     * @return stockReqDtoList
     */
    public List<StockReqDto> findAll() {
        List<Stock> stockList = stockRepo.findAll();
        List<StockReqDto> stockReqDtoList = new ArrayList<>();
        for(Stock stock : stockList){
            StockReqDto stockReqDto = stockReqMapper.map(stock, StockReqDto.class);
            stockReqDtoList.add(stockReqDto);

        }
        return stockReqDtoList;
    }

    public StockReqDto findLastId() {
        List<Stock> stockList = stockRepo.findAll();
        int size = stockList.size() - 1;
        Long lastsize = stockList.get(size).getStockId();
        Stock stock = stockRepo.getOne(lastsize);
        return stockReqMapper.map(stock, StockReqDto.class);
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Get Data by stockId from StockWebClientController
     * @param stockId
     * @return stockReqMapper
     */
    public StockReqDto findById(Long stockId) {

        Optional<Stock>optionalStock = stockRepo.findById(stockId);
        if (!optionalStock.isPresent())
            throw new RecordNotFound(STOCK_NOT_FOUND + stockId);

        Stock stock = stockRepo.getOne(stockId);
        return stockReqMapper.map(stock, StockReqDto.class);
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Update StockWebClientController
     * @param stockId, stockRespDto
     * @return stockRespMapper
     */
    public StockRespDto update(Long stockId, StockRespDto stockRespDto) {
        Optional<Stock>optionalStock = stockRepo.findById(stockId);
        if (!optionalStock.isPresent())
            throw new RecordNotFound(STOCK_NOT_FOUND + stockId);

        Stock stock = stockRespMapper.map(stockRespDto, Stock.class);
        stock.setStockId(stockId);
        Stock stockSaved = stockRepo.save(stock);

        return stockRespMapper.map(stockSaved, StockRespDto.class);
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Create StockWebClientController
     * @param stockRespDto
     * @return stockRespMapper
     */
    public StockRespDto create(StockRespDto stockRespDto) {

        Stock stock = stockRespMapper.map(stockRespDto, Stock.class);
        Stock stockSaved = stockRepo.save(stock);

        return stockRespMapper.map(stockSaved, StockRespDto.class);
    }

    /**
     * @Service
     * This is a Javadoc comment
     * Delete StockWebClientController
     * @param stockId
     */
    public void delete(Long stockId) {
        Optional<Stock>optionalStock = stockRepo.findById(stockId);
        if (!optionalStock.isPresent())
            throw new RecordNotFound(STOCK_NOT_FOUND + stockId);

        stockRepo.deleteById(stockId);
    }

}