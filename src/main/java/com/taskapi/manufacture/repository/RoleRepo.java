package com.taskapi.manufacture.repository;

import com.taskapi.manufacture.model.Role;
import com.taskapi.manufacture.model.dto.CustomerReqDto;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * This is a Javadoc comment
 */
@Repository
public interface RoleRepo extends JpaRepository<Role, Long> {

    @Query(value = "select role from ROLE where ROLE=: role", nativeQuery = true)
    Role findByRole(
            @Param("role") String role);
}

