package com.taskapi.manufacture.repository;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.Users;
import com.taskapi.manufacture.model.dto.CustomerReqDto;
import com.taskapi.manufacture.model.dto.UserReqDto;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * This is a Javadoc comment
 */
@Repository
public interface UserRepo extends JpaRepository<Users, Long> {

    @Query(value = "select username from USERS where USERNAME=:username", nativeQuery = true)
    Users findByUsername(
            @Param("username") String username);
}

