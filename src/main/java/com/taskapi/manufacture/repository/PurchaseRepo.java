package com.taskapi.manufacture.repository;

import com.taskapi.manufacture.model.Purchase;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * This is a Javadoc comment
 */
public interface PurchaseRepo extends JpaRepository<Purchase, Long> {
    @Query(value = "select * from PURCHASE where CUSTOMER_ID=: customerId", nativeQuery = true)
    Purchase findByCustomerId(@Param("customerId") int customerId);

}