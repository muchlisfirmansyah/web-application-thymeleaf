package com.taskapi.manufacture.repository;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.dto.CustomerReqDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * This is a Javadoc comment
 */
@Repository
public interface CustomerRepo extends JpaRepository<Customer, Long> {

    @Query(value = "select * from CUSTOMER where USERNAME=:username and PASSWORD=:password", nativeQuery = true)
    Customer findByUsernameAndPassword(
            @Param("username") String username,
            @Param("password") String password);

    @Query(value = "select * from CUSTOMER where USERNAME=:username", nativeQuery = true)
    Customer findByUsername(
            @Param("username") String username);

}
