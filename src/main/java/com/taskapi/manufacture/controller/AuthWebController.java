package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.Users;
import com.taskapi.manufacture.model.dto.UserRespDto;
import com.taskapi.manufacture.service.CustomerService;

import com.taskapi.manufacture.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Slf4j
@Controller
public class AuthWebController {

    @Autowired
    UserService userService;

    @GetMapping(value = {"/","/signin"})
    public ModelAndView login(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("SignIn");
        return mav;
    }


    @PostMapping(value = "/signin")
    public ModelAndView loginProcess(UserRespDto userRespDto, String cookiesValue, Model model,
                                     RedirectAttributes redir, HttpServletResponse resp){
        ModelAndView mav = new ModelAndView();
        model.addAttribute("cookieValue", cookiesValue);
        log.info("username = " + userRespDto.getUsername() + "password = " + userRespDto.getPassword() );
        redir.addFlashAttribute("customerData", userService.findByUsername(userRespDto.getUsername()));
//                findByUsernameAndPassword(userRespDto.getUsername(), userRespDto.getPassword()));
        Cookie username = new Cookie("username", userRespDto.getUsername());

        username.setMaxAge(24*60*60);

        if (userRespDto.getUsername().toUpperCase().equals("admin")){
            mav.setViewName("redirect:/index");
        }else {
            mav.setViewName("redirect:/home");
        }

        resp.addCookie(username);
        redir.addFlashAttribute("customer", userRespDto.getUsername());
        return mav;
    }

    @GetMapping(value = "/signup")
    public ModelAndView signUp(){
        ModelAndView mav = new ModelAndView();
        Users users = new Users();
        mav.addObject("user", users);
        mav.setViewName("SignUp");
        return mav;
    }


    @PostMapping(value = "/signup")
    public ModelAndView signUpProcess(@Valid UserRespDto userRespDto, BindingResult res){
        ModelAndView mav = new ModelAndView();
        Users usersExist = userService.findByUsername(userRespDto.getUsername());

        if (usersExist != null){
            res.rejectValue("username", "error.user", "This Username has already exist !");
        }

        if (res.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView();

            for (FieldError error : res.getFieldErrors()) {
                log.info(error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField());
                modelAndView.addObject("res", error.getDefaultMessage());
                if (error.getField().equals("username")) {
                    modelAndView.addObject("errorForUsername", error.getDefaultMessage());
                }
                if (error.getField().equals("email")) {
                    modelAndView.addObject("errorForEmail", error.getDefaultMessage());
                }
                if (error.getField().equals("password")) {
                    modelAndView.addObject("errorForPassword", error.getDefaultMessage());
                }
            }
            modelAndView.setViewName("SignUp");
            return modelAndView;
        }
        userService.userCreate(userRespDto);

        mav.addObject("msg", "User has been registered successfully !");
        mav.addObject("user", new Users());
        mav.setViewName("SignIn");
        return mav;
    }

    @GetMapping("/error")
    public ModelAndView error(){
        ModelAndView mav = new ModelAndView();
        String errorMessage = "You are not authorize for the requested data";
        mav.addObject("errorMsg", errorMessage);
        mav.setViewName("AccessDenied");

        return mav;
    }

}
