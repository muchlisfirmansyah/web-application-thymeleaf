package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.dto.PurchaseRespDto;
import com.taskapi.manufacture.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PurchaseWebController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping (value = "purchaseList")
    public ModelAndView purchaseList() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("purchases", purchaseService.findAll());
        mav.setViewName("admin-purchase-list");
        return mav;
    }

//    @GetMapping(value = "purchase-add")
//    public ModelAndView purchaseAdd() {
//        ModelAndView mav = new ModelAndView();
//        mav.setViewName("admin-cust-add");
//        return mav;
//    }
//
//    @PostMapping(value = "purchase-add")
//    public ModelAndView puchasePOST(PurchaseRespDto purchaseRespDto){
//        ModelAndView mav = new ModelAndView();
//        purchaseService.create(purchaseRespDto);
//        mav.setViewName("admin-cust-add");
//        return mav;
//    }

    @GetMapping(value = "/get-purc/{Id}")
    public ModelAndView purchaseDet(@PathVariable("Id") Long purchaseId){
        ModelAndView mav = new ModelAndView("admin-stock-det");
        mav.addObject("purchase", purchaseService.findById(purchaseId));
        return mav;
    }
}
