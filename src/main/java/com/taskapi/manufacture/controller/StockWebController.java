package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.dto.CustomerRespDto;
import com.taskapi.manufacture.model.dto.StockRespDto;
import com.taskapi.manufacture.service.CustomerService;
import com.taskapi.manufacture.service.StockService;
import io.swagger.models.Model;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;

@Slf4j
@Controller
public class StockWebController {

    @Autowired
    StockService stockService;

    @GetMapping(value = "stock-add")
    public ModelAndView stockAdd(StockRespDto stockRespDto, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("createdStock", stockRespDto);
        mav.addObject("username", cookieValue);
        mav.setViewName("admin-stock-add");
        return mav;
    }

    @PostMapping(value = "stock-add")
    public ModelAndView stockPOST(@Valid StockRespDto stockRespDto, BindingResult res){
        ModelAndView mav = new ModelAndView();

        if (res.hasErrors()) {
            log.info("isi res " + res.toString());
            ModelAndView modelAndView = new ModelAndView();

            for (FieldError error : res.getFieldErrors()) {
                log.info(error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField());
                modelAndView.addObject("res", error.getDefaultMessage());
                if (error.getField().equals("name")) {
                    modelAndView.addObject("errorForName", error.getDefaultMessage());
                }
                if (error.getField().equals("qtyStock")) {
                    modelAndView.addObject("errorForQtyStock", error.getDefaultMessage());
                }
                if (error.getField().equals("unitPrice")) {
                    modelAndView.addObject("errorForUnitPrice", error.getDefaultMessage());
                }
                if (error.getField().equals("storageLocation")) {
                    modelAndView.addObject("errorForStorageLocation", error.getDefaultMessage());
                }
            }
            modelAndView.addObject("errorStatus","Status : Failed");
            modelAndView.addObject("createdStock", stockRespDto);
            modelAndView.setViewName("admin-stock-add");
            return modelAndView;
        }
        log.info("masuk create");
        stockService.create(stockRespDto);

        mav.addObject("errorStatus","Status : Success");
        mav.addObject("createdStock", stockRespDto);
        mav.setViewName("admin-stock-add");

        return mav;
    }

    @GetMapping (value = "/get/{Id}")
    public ModelAndView stockDetail(@PathVariable("Id") Long stockId, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView("admin-stock-det");
        mav.addObject("stock", stockService.findById(stockId));
        mav.addObject("username", cookieValue);
        return mav;
    }

    @GetMapping(value = "/delete/{Id}")
    public RedirectView stockDelete(@PathVariable("Id") Long stockId, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView("admin-stock-det");
        stockService.delete(stockId);
        return new RedirectView("/task/index");
    }

    @GetMapping(value = "/update/{Id}")
    public ModelAndView updateStock(@PathVariable("Id") Long stockId, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView();
        mav.addObject("stock", stockService.findById(stockId));
        mav.setViewName("admin-stock-update");
        mav.addObject("username", cookieValue);
        return mav;
    }

    @PostMapping(value = "/update/{Id}")
    public ModelAndView updateStockPOST(@PathVariable("Id") Long stockId, StockRespDto stockRespDto, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue, BindingResult res){
        ModelAndView mav = new ModelAndView();

        if (res.hasErrors()) {
            log.info("isi res " + res.toString());
            ModelAndView modelAndView = new ModelAndView();

            for (FieldError error : res.getFieldErrors()) {
                log.info(error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField());
                modelAndView.addObject("res", error.getDefaultMessage());
                if (error.getField().equals("name")) {
                    modelAndView.addObject("errorForName", error.getDefaultMessage());
                }
                if (error.getField().equals("qtyStock")) {
                    modelAndView.addObject("errorForQtyStock", error.getDefaultMessage());
                }
                if (error.getField().equals("unitPrice")) {
                    modelAndView.addObject("errorForUnitPrice", error.getDefaultMessage());
                }
                if (error.getField().equals("storageLocation")) {
                    modelAndView.addObject("errorForStorageLocation", error.getDefaultMessage());
                }
            }
            modelAndView.setViewName("admin-stock-update");
            modelAndView.addObject("username", cookieValue);
            return modelAndView;
        }
        mav.setViewName("admin-stock-update");
        mav.addObject("username", cookieValue);
        stockService.update(stockId, stockRespDto);
        return mav;



    }

}
