package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.dto.CustomerRespDto;
import com.taskapi.manufacture.model.dto.StockRespDto;
import com.taskapi.manufacture.service.CustomerService;
import io.swagger.models.Model;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@Slf4j
@Controller
public class CustomerWebController {

    @Autowired
    CustomerService customerService;

    @GetMapping(value = "custList")
    public ModelAndView customerList(@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue) {
        ModelAndView mav = new ModelAndView("admin-cust-list");
        mav.addObject("username", cookieValue);
        mav.addObject("customers", customerService.findAll());
        return mav;
    }

    @GetMapping(value = "customer-add")
    public ModelAndView customerAdd(@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue, CustomerRespDto customerRespDto) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("username", cookieValue);
        mav.addObject("createdCustomer",customerRespDto);
        mav.setViewName("admin-cust-add");
        return mav;
    }

    @PostMapping(value = "customer-add")
    public ModelAndView customerPOST(@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue, CustomerRespDto customerRespDto, BindingResult res){
        ModelAndView mav = new ModelAndView();

        if (res.hasErrors()) {
            log.info("isi res " + res.toString());
            ModelAndView modelAndView = new ModelAndView();

            for (FieldError error : res.getFieldErrors()) {
                log.info(error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField());
                modelAndView.addObject("res", error.getDefaultMessage());
                if (error.getField().equals("companyName")) {
                    modelAndView.addObject("errorForCompanyName", error.getDefaultMessage());
                }
                if (error.getField().equals("established")) {
                    modelAndView.addObject("errorForEstablished", error.getDefaultMessage());
                }
                if (error.getField().equals("address")) {
                    modelAndView.addObject("errorForAddress", error.getDefaultMessage());
                }
                if (error.getField().equals("companyPhone")) {
                    modelAndView.addObject("errorForCompanyPhone", error.getDefaultMessage());
                }
                if (error.getField().equals("email")) {
                    modelAndView.addObject("errorForEmail", error.getDefaultMessage());
                }
                if (error.getField().equals("status")) {
                    modelAndView.addObject("errorForStatus", error.getDefaultMessage());
                }
                if (error.getField().equals("accountBalance")) {
                    modelAndView.addObject("errorForAccountBalance", error.getDefaultMessage());
                }
            }

            modelAndView.addObject("errorStatus","Status : Failed");
            modelAndView.addObject("createdCustomer", customerRespDto);
            modelAndView.setViewName("admin-cust-add");
            return modelAndView;
        }
        customerService.create(customerRespDto);
        mav.addObject("errorStatus","Status : Success");
        mav.addObject("username", cookieValue);
        mav.addObject("createdCustomer", customerRespDto);
        mav.setViewName("admin-cust-add");
        return mav;
    }

    @GetMapping(value = "/get-cust/{Id}")
    public ModelAndView customerDetail(@PathVariable("Id") Long customerId, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView("admin-cust-det");
        mav.addObject("username", cookieValue);
        mav.addObject("customer", customerService.findById(customerId));
        return mav;
    }

    @GetMapping(value = "/delete-cust/{Id}")
    public RedirectView customerDelete(@PathVariable("Id") Long customerId, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView("admin-cust-det");
        mav.addObject("username", cookieValue);
        customerService.delete(customerId);
        return new RedirectView("/task/custList");
    }

    @GetMapping(value = "/update-cust/{Id}")
    public ModelAndView updatecustomer(@PathVariable("Id") Long customerId,  @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView();
        mav.addObject("username", cookieValue);
        mav.addObject("customer", customerService.findById(customerId));
        mav.setViewName("admin-cust-update");
        return mav;
    }

    @PostMapping(value = "/update-cust/{Id}")
    public ModelAndView updatecustomerPOST(@PathVariable("Id") Long customerId, CustomerRespDto customerRespDto, BindingResult res, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){

        ModelAndView mav = new ModelAndView();

        if (res.hasErrors()) {
            log.info("isi res " + res.toString());
            ModelAndView modelAndView = new ModelAndView();

            for (FieldError error : res.getFieldErrors()) {
                log.info(error.getObjectName() + " - " + error.getDefaultMessage() + " - " + error.getField());
                modelAndView.addObject("res", error.getDefaultMessage());
                if (error.getField().equals("companyName")) {
                    modelAndView.addObject("errorForCompanyName", error.getDefaultMessage());
                }
                if (error.getField().equals("established")) {
                    modelAndView.addObject("errorForEstablished", error.getDefaultMessage());
                }
                if (error.getField().equals("address")) {
                    modelAndView.addObject("errorForAddress", error.getDefaultMessage());
                }
                if (error.getField().equals("companyPhone")) {
                    modelAndView.addObject("errorForCompanyPhone", error.getDefaultMessage());
                }
                if (error.getField().equals("email")) {
                    modelAndView.addObject("errorForEmail", error.getDefaultMessage());
                }
                if (error.getField().equals("status")) {
                    modelAndView.addObject("errorForStatus", error.getDefaultMessage());
                }
                if (error.getField().equals("accountBalance")) {
                    modelAndView.addObject("errorForAccountBalance", error.getDefaultMessage());
                }
            }

            modelAndView.setViewName("admin-cust-update");
            return modelAndView;
        }
        customerService.update(customerId, customerRespDto);
        mav.setViewName("admin-cust-update");
        return mav;
    }


}
