package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.dto.StockRespDto;
import com.taskapi.manufacture.service.StockService;
import io.swagger.models.Model;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("stock")
public class StockWebClientController {

    @Autowired
    StockService stockService;

    @GetMapping("view")
    public ModelAndView showAll(@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView("client-stock-list");
        mav.addObject("stocks", stockService.findAll());
        mav.addObject("username", cookieValue);
        return mav;
    }

    @GetMapping("view/{Id}")
    public ModelAndView stockDetail(@PathVariable("Id") Long stockId, @CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView("client-stock-det");
        mav.addObject("stock", stockService.findById(stockId));
        log.info("ISI >> " + stockService.findById(stockId).toString());
        mav.addObject("username", cookieValue);
        return mav;
    }

    @GetMapping("add")
    public ModelAndView customerAdd(@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("stockAdd");
        mav.addObject("username", cookieValue);
        return mav;
    }

    @PostMapping("add")
    public ModelAndView customerPOST(@Valid StockRespDto stockRespDto,@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){

        ModelAndView mav = new ModelAndView();
        stockService.create(stockRespDto);
        mav.addObject("stocks", stockService.findAll());
        mav.addObject("username", cookieValue);
        mav.setViewName("stock");
        return mav;
    }

}
