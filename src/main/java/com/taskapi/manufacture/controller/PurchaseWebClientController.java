package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.dto.PurchaseRespDto;
import com.taskapi.manufacture.service.PurchaseService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
public class PurchaseWebClientController {

    @Autowired
    PurchaseService purchaseService;


    @PostMapping(value = "purchase-add")
    public ModelAndView puchasePOST(PurchaseRespDto purchaseRespDto,@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue){
        ModelAndView mav = new ModelAndView();
        purchaseRespDto.setUsername(cookieValue);
        mav.addObject("username", cookieValue);
        mav.addObject("purchase",purchaseService.create(purchaseRespDto));
        mav.setViewName("client-stock-success");
        return mav;
    }

//    @GetMapping(value = "/get-purc/{Id}")
//    public ModelAndView purchaseDet(@PathVariable("Id") Long purchaseId){
//        ModelAndView mav = new ModelAndView("admin-stock-det");
//        mav.addObject("purchase", purchaseService.findById(purchaseId));
//        return mav;
//    }
}
