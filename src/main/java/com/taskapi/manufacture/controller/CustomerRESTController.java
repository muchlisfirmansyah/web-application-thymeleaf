package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.dto.CustomerRespDto;
import com.taskapi.manufacture.service.CustomerService;
import com.taskapi.manufacture.util.Response;
import io.swagger.models.Model;
import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * This is a Javadoc comment
 */
@Slf4j
@Controller
@RequestMapping(value = "customer")
public class CustomerRESTController {
    @Autowired
    CustomerService customerService;

    @GetMapping("customerShowAll")
    public ModelAndView showAll(Model model){
        ModelAndView mav = new ModelAndView("customer");
        mav.addObject("customers", customerService.findAll());
        return mav;
    }

    @GetMapping("tambahCustomer")
    public ModelAndView customerAdd( ) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("customerAdd");
        return mav;
    }

    @PostMapping("tambahCustomer")
    public ModelAndView customerPOST(@Valid CustomerRespDto customerRespDto){

        ModelAndView mav = new ModelAndView();
        customerService.create(customerRespDto);
        mav.addObject("customers", customerService.findAll());
        mav.setViewName("customer");
        return mav;
    }

    @PostMapping
    ResponseEntity<Response> addCustomer(@RequestBody @Valid CustomerRespDto customerRespDto) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Add Customer Success !");
        response.setData(customerService.create(customerRespDto));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @GetMapping
    ResponseEntity<Response> findAllCustomer() {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data retrieved !");
        response.setData(customerService.findAll());

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @GetMapping(value = "/{id}")
    ResponseEntity<Response> findCustomerById(@PathVariable("id") Long customerId) {
        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Successfully retrieve by Customer ID : " + customerId );
        response.setData(customerService.findById(customerId));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @PutMapping(value = "/{id}")
    ResponseEntity<Response> updateCustomer(@PathVariable("id") Long customerId, @RequestBody @Validated CustomerRespDto customerRespDto) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data with ID : " + customerId + " updated !");
        response.setData(customerService.update(customerId, customerRespDto));

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);
    }

    @DeleteMapping(value = "/{id}")
    ResponseEntity<Response> deleteCustomerById(@PathVariable("id") Long customerId) {

        String methodName = new Throwable().getStackTrace()[0].getMethodName();

        Response response = new Response();
        response.setService("Controller method loc : " + methodName);
        response.setMessage("Data with ID " + customerId + " deleted !");
        response.setData(customerService.findById(customerId));

        customerService.delete(customerId);

        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON).body(response);

    }
}

