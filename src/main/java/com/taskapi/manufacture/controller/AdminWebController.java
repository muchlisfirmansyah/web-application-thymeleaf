package com.taskapi.manufacture.controller;

import com.taskapi.manufacture.model.Users;
import com.taskapi.manufacture.model.dto.UserReqDto;
import com.taskapi.manufacture.service.StockService;
import com.taskapi.manufacture.service.UserService;
import io.swagger.models.Model;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Slf4j
@Controller
public class AdminWebController {

    @Autowired
    StockService stockService;

    @Autowired
    UserService userService;

//
//    @GetMapping(value = "index")
//    public ModelAndView index(@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue) {
//        ModelAndView mav = new ModelAndView();
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        UserReqDto user = userService.findByUsername(auth.getName());
//        mav.setViewName("dashboard");
//        mav.addObject("username", cookieValue);
//        mav.addObject("stocks", stockService.findAll());
//        return mav;
//    }

    @GetMapping(value = "index")
    public ModelAndView index(@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue) {
        ModelAndView mav = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Users user = userService.findByUsername(auth.getName());

        mav.setViewName("dashboard");
//        mav.addObject("username", user.getUsername());
        mav.addObject("stocks", stockService.findAll());
        return mav;
    }

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public ModelAndView homeClient(@CookieValue(value = "username", defaultValue = "defaultCookieValue") String cookieValue, HttpSession session) {
        ModelAndView mav = new ModelAndView();

        log.info("SESSION " + session.getId());
        mav.setViewName("index");
//        mav.addObject("session", session.getId());
        mav.addObject("username", cookieValue);
        mav.addObject("stocks", stockService.findAll());
        return mav;
    }

}
