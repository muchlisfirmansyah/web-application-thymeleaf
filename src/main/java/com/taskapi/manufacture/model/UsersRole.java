package com.taskapi.manufacture.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * This is a Javadoc comment
 */
@Getter
@Setter
@Entity
@Table(name = "USER_ROLE")

public class UsersRole implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "USER_ID")
    private Long userId;

    @Id
    @Column(name = "ROLE_ID")
    private Long roleId;


}
