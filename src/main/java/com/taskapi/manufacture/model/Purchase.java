package com.taskapi.manufacture.model;

import java.io.Serializable;
import java.sql.Date;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is a Javadoc comment
 */
@Getter
@Setter
@Entity
@Table(name = "PURCHASE")
public class Purchase implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PURCHASE_ID", nullable = false)
    private Long purchaseId;



    @Column(name = "USERNAME", nullable = false)
    private String username;

    @Column(name = "STOCK_ID", nullable = false)
    private Long stockId;

    @Column(name = "QTY_PURCHASE")
    private Integer qtyPurchase;

    @Column(name = "TOTAL_PRICE")
    private Integer totalPrice;

    @DateTimeFormat(pattern = "HH:mm:ss")
    @Column(name = "CREATED_DATE")
    private Date createdDate;
}



