package com.taskapi.manufacture.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * This is a Javadoc comment
 */
@Getter
@Setter
@Entity
@Table(name = "ROLE")

public class Role implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROLE_ID")
    private Long roleId;

    @Column(name = "ROLE")
    private String role;


}
