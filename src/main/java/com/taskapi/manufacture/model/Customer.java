package com.taskapi.manufacture.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Getter
@Setter
@Entity
@Table(name = "Customer")

public class Customer implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CUSTOMER_ID")
    private Long customerId;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "ESTABLISHED")
    private Date established;

    @Column(name="ADDRESS")
    private String address;

    @Column(name = "COMPANY_PHONE")
    private String companyPhone;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "ACCOUNT_BALANCE")
    private Integer accountBalance;

}
