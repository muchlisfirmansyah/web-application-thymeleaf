package com.taskapi.manufacture.model.mapper;

import com.taskapi.manufacture.model.Stock;
import com.taskapi.manufacture.model.dto.StockReqDto;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

/**
 * StockWebClientController Request Mapper
 */
@Component
public class StockReqMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(StockReqDto.class, Stock.class)
                .byDefault()
                .register();
    }
}
