package com.taskapi.manufacture.model.mapper;

import com.taskapi.manufacture.model.Purchase;
import com.taskapi.manufacture.model.dto.PurchaseRespDto;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;

import org.springframework.stereotype.Component;

/**
 * Purchase Response Mapper
 */
@Component
public class PurchaseRespMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Purchase.class, PurchaseRespDto.class)
                .byDefault()
                .exclude("purchaseId")
                .register();
    }
}
