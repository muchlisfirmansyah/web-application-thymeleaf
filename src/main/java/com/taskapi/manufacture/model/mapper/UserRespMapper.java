package com.taskapi.manufacture.model.mapper;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.dto.UserReqDto;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class UserRespMapper extends ConfigurableMapper{

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Customer.class, UserReqDto.class)
//                .exclude("companyName")
//                .exclude("established")
//                .exclude("address")
//                .exclude("companyPhone")
//                .exclude("status")
//                .exclude("accountBalance")
                .byDefault()
                .register();
    }
}

