package com.taskapi.manufacture.model.mapper;

import com.taskapi.manufacture.model.Customer;
import com.taskapi.manufacture.model.dto.CustomerRespDto;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

/**
 * Customer Response Mapper
 */
@Component
public class CustomerRespMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(Customer.class, CustomerRespDto.class)
                .byDefault()
                .register();
    }
}
