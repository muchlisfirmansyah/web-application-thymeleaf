package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;


/**
 * Customer DTO
 */
@Data
public class CustomerRespDto {

    private Long Id;

    private String username;

    private String password;

    @NotEmpty(message = "Name is a required")
    private String companyName;

    private Date established;

    @NotEmpty(message = "Address is required")
    private String address;

    @NotEmpty(message = "Company Phone is required")
    private String companyPhone;

    @Email(message = "Please check your Email format")
    private String email;

//    @NotNull(message = "Status is a required")
    private String status;

    @NotNull(message = "Account Balance is a required")
    @PositiveOrZero(message = "Account Balance must be greater than or equal to 0")
    private Integer accountBalance;

}
