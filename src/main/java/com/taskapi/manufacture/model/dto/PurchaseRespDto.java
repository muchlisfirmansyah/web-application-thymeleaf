package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.util.Date;

/**
 * This is a Javadoc comment
 */
@Data
@NoArgsConstructor
public class PurchaseRespDto {

//    @NotNull(message = "Customer ID is a required")
//    private Long customerId;

    private String username;

    @NotNull(message = "StockWebClientController ID is a required")
    private Long stockId;

    @NotNull(message = "Quantity Purchase is a required")
    @PositiveOrZero(message = "Quantity Purchase must be greater than or equal to 0")
    private Integer qtyPurchase;

    private Integer totalPrice;

    @DateTimeFormat
    private Date createdDate;

}
