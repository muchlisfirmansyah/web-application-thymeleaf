package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

/**
 * Customer DTO
 */
@Data
@NoArgsConstructor
public class CustomerReqDto {

    private Long customerId;

    private String username;

    private String password;

    private String companyName;

    private Date established;

    private String address;

    private String companyPhone;

    private String email;

    private String status;

    private Integer accountBalance;

}
