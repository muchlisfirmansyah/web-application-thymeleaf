package com.taskapi.manufacture.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

/**
 * This is a Javadoc comment
 */
@Data
public class StockRespDto {

    @NotEmpty(message = "Name is a required")
    @Size(min = 1, max = 25, message = "Stock Name character must more than 1 and less than 25")
    private String name;

    @NotNull(message = "Qty Stock is a required")
    @PositiveOrZero(message = "Quantity Stock must be greater than or equal to 0")
    private Integer qtyStock;

    @NotEmpty(message = "Measurement Unit is a required")
    private String measurementUnit;

    private String status;

    @NotNull(message = "Unit Price is a required")
    @PositiveOrZero(message = "Unit Price must be greater than or equal to 0")
    private Integer unitPrice;

    @NotEmpty(message = "Storage Location is a required")
    private String storageLocation;

    private String desc;

}
