package com.taskapi.manufacture.model.dto;

import com.taskapi.manufacture.model.Role;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
public class UserReqDto {

    private Long userId;

    @NotEmpty
    @Size(min = 1, max = 12, message = "Username character must more than 1 and less than 12")
    private String username;

    @NotEmpty
    @Size(min = 1, message = "Password must more than 1 character")
    private String password;

    @Email(message = "Email should be valid")
    private String email;

    private Set<Role> role;

    private boolean isActive;

}
