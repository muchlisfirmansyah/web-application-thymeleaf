package com.taskapi.manufacture.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * This is a Javadoc comment
 */
@Getter
@Setter
@Entity
@Table(name = "STOCK")
public class Stock implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "STOCK_ID")
    private Long stockId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "QTY_STOCK")
    private Integer qtyStock;

    @Column(name = "MEASURE_UNIT")
    @Enumerated(EnumType.STRING)
    private MeasurementUnit measurementUnit;

    public enum MeasurementUnit {
        Bottle("Bottle"),
        Capsule("Capsule"),
        Tablet("Tablet"),
        Pieces("Pieces"),
        Dos("Dos"),
        Box("Box");

        public final String label;

        /**
         * @param label
         */
        MeasurementUnit(String label) {

            this.label = label;
        }
    }

    @Column(name = "STATUS")
    private String status;

    @Column(name = "UNIT_PRICE")
    private Integer unitPrice;

    @Column(name = "STORAGE_LOC")
    private String storageLocation;

    @Column(name = "DESCRIPTION")
    private String desc;

}
