insert into STOCK (STOCK_ID, MEASURE_UNIT, NAME, QTY_STOCK, STATUS, UNIT_PRICE, STORAGE_LOC, DESCRIPTION) values
(1, 'Bottle', 'Fish oil with Omega-3', 53, 'Ready', 20000, 'Jakarta', 'Omega-3 fish oil contains both docosahexaenoic acid (DHA) and eicosapentaenoic acid (EPA). Omega-3 fatty acids are essential nutrients that are important in preventing and managing heart disease.'),
(2, 'Box', 'HerbalNutri Complex', 61, 'Ready', 78690, 'Jakarta',''),
(3, 'Bottle', 'Bio-Multivitamin', 42, 'Ready',  145000, 'Jakarta','Bio Multivitamins will give your body more energy, vitality, and strength. Bio Multivitamins is a good aid in cases of back pain , leg pain, discouragement, anemia, dehydration, bleeding from nose, white spots by weakness, fatigue and sleepless.'),
(4, 'Box', 'Gold Cross Vitamin C ', 55, 'Ready',  1400000, 'Jakarta','Vitamin C is required to maintain healthy body tissues and is known for its antioxidant properties. It is a popular supplement especially during the winter months.'),
(5, 'Bottle', 'Ketonal 30ml', 140, 'Ready', 240690, 'Karawang','Letonal 100 mg Tablet is a potassium-sparing diuretic used to treat high blood pressure, heart failure, and swelling (edema) caused by certain conditions (such as congestive heart failure, or liver cirrhosis)');

insert into CUSTOMER (CUSTOMER_ID, ACCOUNT_BALANCE, COMPANY_NAME, STATUS, ESTABLISHED, COMPANY_PHONE, EMAIL, USERNAME, PASSWORD) values
(1, 0,'admin','AKTIF','2019-05-10','-','-','admin','admin'),
(2, 86000000, 'Innogene Kalbiotech Pte. Ltd.' , 'AKTIF', '2019-05-10', '0214335667', 'abc@doku.com','innogene', 'password'),
(3, 78050000, 'PT Pharma Metric Labs', 'AKTIF', '2019-05-10', '0214335667', 'abc@doku.com','pharma', 'password'),
(4, 58040000, 'PT Kalbio Global Medika', 'AKTIF', '2019-05-10', '0214335667', 'abc@doku.com','kalbio', 'password'),
(5, 45000000, 'Stem Cell & Cancer Institute', 'AKTIF', '2019-05-10', '0214335667', 'abc@doku.com', 'stem', 'password');

insert into PURCHASE(PURCHASE_ID, CREATED_DATE, USERNAME, QTY_PURCHASE, STOCK_ID, TOTAL_PRICE) values
(1, '2019-05-10', 'kalbio', 64, 1, 1280000),
(2, '2019-05-12', 'kalbio', 56, 2, 4406640),
(3, '2019-06-10', 'pharma', 42, 3,  6090000),
(4, '2019-06-08', 'stem', 12, 4,  8664840);


insert into USERS(ID, USERNAME, PASSWORD, IS_ACTIVE) values
(1, 'kalbio', '$2a$10$n.H/TFNggizmkrEEDVt7B.tTCQ/Gh16xgKuaZHXAlJAy.bp0.HpZW', 'true'),
(2, 'admin', '$2a$10$ZE26wi8WLn62ImIvWaC2MO5gUoKXLuC.VZec4oV9KJt7a5QnrZnTi', 'true');

insert into ROLE(ROLE_ID, ROLE) values
(1, 'ADMIN'),
(2, 'CUSTOMER');

insert into USER_ROLE(USER_ID, ROLE_ID) values
(1, 2),
(2, 1);
